package com.mcecraft.androidapp.model;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Channel {

	private final ArrayList<Message> messages = new ArrayList<>();
	private int chatImage;
	private String name;
}
