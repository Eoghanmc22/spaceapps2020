package com.mcecraft.androidapp.model;

import lombok.Data;

@Data
public class Message {
	final String message;
	final User sender;
	final long createdAt;
}
