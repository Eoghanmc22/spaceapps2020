package com.mcecraft.androidapp.model;

import lombok.Data;

@Data
public class User {
	final int userId;
	final String nickname;
	final String description;
	final int profilePic;
}
