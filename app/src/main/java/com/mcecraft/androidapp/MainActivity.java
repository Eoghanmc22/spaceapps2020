package com.mcecraft.androidapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mcecraft.androidapp.model.Channel;
import com.mcecraft.androidapp.model.User;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

import lombok.Getter;

public class MainActivity extends AppCompatActivity {

	@Getter
	HashMap<UUID, Channel> tempChannels = new HashMap<>();
	public FusedLocationProviderClient fusedLocationClient;
	public ArrayList<Channel> channels = new ArrayList<>();
	public ArrayList<String> channelNames = new ArrayList<>();
	public Address loc;
	public User self = new User(0, "You", "I like hacking", R.mipmap.flower4);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		boolean doQuiz = true;
		if (doQuiz && !getIntent().getBooleanExtra("noQuiz", false)) {
			Intent intent = new Intent(this, QuizActivity.class);
			startActivity(intent);
			return;
		}
		fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			fusedLocationClient.getLastLocation().addOnSuccessListener((loc) -> {
				if (Geocoder.isPresent()) {
					Geocoder geoCoder = new Geocoder(this, Locale.ENGLISH);
					try {
						this.loc = geoCoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1).get(0);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
		}
		setContentView(R.layout.activity_main);
		BottomNavigationView navView = findViewById(R.id.nav_view);
		// Passing each menu ID as a set of Ids because each
		// menu should be considered as top level destinations.
		AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
				R.id.navigation_home, R.id.navigation_chats, R.id.navigation_garden)
				.build();
		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
		NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
		NavigationUI.setupWithNavController(navView, navController);
	}

}