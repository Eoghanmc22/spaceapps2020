package com.mcecraft.androidapp.ui;

import android.graphics.Matrix;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.mcecraft.androidapp.MainActivity;
import com.mcecraft.androidapp.R;
import com.mcecraft.androidapp.adapters.MessageListAdapter;
import com.mcecraft.androidapp.adapters.PeopleListAdapter;
import com.mcecraft.androidapp.model.User;

public class HomeFragment extends Fragment {

	private RecyclerView mRecommendedRecycler;
	private PeopleListAdapter mRecomendedAdapter;
	private MainActivity activity = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		activity = (MainActivity) getActivity();
		View root = inflater.inflate(R.layout.fragment_home, container, false);
		mRecomendedAdapter = new PeopleListAdapter(getActivity());
		RecyclerView recommended = root.findViewById(R.id.recommendedPeople);
		LinearLayoutManager manager = new LinearLayoutManager(getActivity());
		recommended.setLayoutManager(manager);
		recommended.setAdapter(mRecomendedAdapter);
		if (!activity.channelNames.contains("Jasmine"))
			mRecomendedAdapter.addPerson(new User(1, "Jasmine", "Don't leaf me alone in quarantine! I'm rooting for everyone", R.mipmap.flower2));
		if (!activity.channelNames.contains("Wilson"))
			mRecomendedAdapter.addPerson(new User(3, "Wilson", "How’s it growing bud-dy? Let’s let a friendship blossom", R.mipmap.flower5));
		if (!activity.channelNames.contains("Sam"))
			mRecomendedAdapter.addPerson(new User(7, "Sam", "omg imagine if a flower was interested in \"STEM\" lol", R.mipmap.flower3));
		if (!activity.channelNames.contains("Owen"))
			mRecomendedAdapter.addPerson(new User(5, "Owen", "Peony for your thoughts, What's up buttercup, have a marvelous daisy, etc", R.mipmap.flower4));
		ImageView imageView = root.findViewById(R.id.pointer);
		RotateAnimation toRotate = new RotateAnimation(0, 40, 350, imageView.getDrawable().getBounds().height()/2f);
		toRotate.setDuration(1000);
		toRotate.setFillAfter(true);
		imageView.setAnimation(toRotate);
		return root;
	}

}