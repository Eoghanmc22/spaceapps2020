package com.mcecraft.androidapp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mcecraft.androidapp.MainActivity;
import com.mcecraft.androidapp.R;
import com.mcecraft.androidapp.adapters.ChatListAdapter;
import com.mcecraft.androidapp.model.Channel;
import com.mcecraft.androidapp.model.Message;
import com.mcecraft.androidapp.model.User;

public class ChatsFragment extends Fragment {
	private RecyclerView mChatRecycler;
	private MainActivity activity = null;
	private ChatListAdapter mChatListAdapter;

	public void send(View v) {

	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
	                         @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		activity = (MainActivity) getActivity();
		mChatListAdapter = new ChatListAdapter(getActivity());
		View root = inflater.inflate(R.layout.fragment_chats, container, false);
		mChatRecycler = root.findViewById(R.id.chats);
		mChatRecycler.setAdapter(mChatListAdapter);
		LinearLayoutManager manager = new LinearLayoutManager(getActivity());
		mChatRecycler.setLayoutManager(manager);
		mChatRecycler.setHasFixedSize(true);
		mChatListAdapter.addChannels(activity.channels);
		/*
		User one = new User(0, "test", "test", R.mipmap.flower4);
		User two = new User(1, "tester", "test", R.mipmap.flower5);
		Message one1 = new Message("hi", one, System.currentTimeMillis());
		Message two1 = new Message("hi there,", two, System.currentTimeMillis());
		Message one2 = new Message("hello", one, System.currentTimeMillis());
		Message two2 = new Message("yes", two, System.currentTimeMillis());
		Channel c1 = new Channel(two.getProfilePic(), "test1");
		c1.getMessages().add(one1);
		c1.getMessages().add(two1);
		Channel c2 = new Channel(one.getProfilePic(), "test2");
		c2.getMessages().add(two2);
		c2.getMessages().add(one2);
		mChatListAdapter.addChannel(c1);
		mChatListAdapter.addChannel(c2);
		*/
		return root;
	}
}
