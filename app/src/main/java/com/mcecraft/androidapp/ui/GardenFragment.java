package com.mcecraft.androidapp.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mcecraft.androidapp.MainActivity;
import com.mcecraft.androidapp.R;

public class GardenFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		MainActivity activity = (MainActivity) getActivity();
		View root = inflater.inflate(R.layout.fragment_garden, container, false);
		ImageView owen = root.findViewById(R.id.owen);
		ImageView jasmine = root.findViewById(R.id.jasmine);
		ImageView sam = root.findViewById(R.id.sam);
		ImageView wilson = root.findViewById(R.id.wilson);
		if (activity.channelNames.contains("Owen"))
			owen.setVisibility(View.VISIBLE);
		if (activity.channelNames.contains("Jasmine"))
			jasmine.setVisibility(View.VISIBLE);
		if (activity.channelNames.contains("Sam"))
			sam.setVisibility(View.VISIBLE);
		if (activity.channelNames.contains("Wilson"))
			wilson.setVisibility(View.VISIBLE);
		return root;
	}

}