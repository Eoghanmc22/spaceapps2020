package com.mcecraft.androidapp.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.mcecraft.androidapp.MainActivity;
import com.mcecraft.androidapp.R;
import com.mcecraft.androidapp.adapters.MessageListAdapter;
import com.mcecraft.androidapp.model.Channel;
import com.mcecraft.androidapp.model.Message;
import com.mcecraft.androidapp.model.User;

import java.util.UUID;

public class ChatFragment extends Fragment {

	private RecyclerView mChatRecycler;
	private MessageListAdapter mChatAdapter;
	private MainActivity activity = null;
	private Channel channel = null;
	TextInputEditText input;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
	                         @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		activity = (MainActivity) getActivity();
		channel = activity.getTempChannels().remove(UUID.fromString(getArguments().getString("tempChannel")));
		if (channel == null) {
			return null;
		}
		mChatAdapter = new MessageListAdapter(getActivity());
		View root = inflater.inflate(R.layout.fragment_chat, container, false);
		final FloatingActionButton sendButton = root.findViewById(R.id.sendButton);
		sendButton.setVisibility(View.GONE);
		input = root.findViewById(R.id.textInput);
		mChatRecycler = root.findViewById(R.id.chat);
		mChatRecycler.setAdapter(mChatAdapter);
		LinearLayoutManager manager = new LinearLayoutManager(getActivity());
		manager.setStackFromEnd(true);
		mChatRecycler.setLayoutManager(manager);
		mChatRecycler.setHasFixedSize(true);
		mChatAdapter.addMessages(channel.getMessages());
		mChatRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {

			@Override
			public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
				if (sendButton.getVisibility() != View.GONE)
					sendButton.setAlpha(1f);
				sendButton.animate().alpha(0f).setDuration(500L).setListener(new AnimatorListenerAdapter() {

					@Override
					public void onAnimationEnd(Animator animation) {
						sendButton.setVisibility(View.GONE);
					}
				});
			}
		});
		input.addTextChangedListener(new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (sendButton.getVisibility() != View.VISIBLE) {
					sendButton.setAlpha(0f);
					sendButton.setVisibility(View.VISIBLE);
					sendButton.animate().alpha(1f).setDuration(500L).setListener(new AnimatorListenerAdapter() {

						@Override
						public void onAnimationStart(Animator animation) {
							sendButton.setVisibility(View.VISIBLE);
						}
					});
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		input.setOnClickListener(v -> {
			if (sendButton.getVisibility() != View.VISIBLE) {
				sendButton.setAlpha(0f);
				sendButton.setVisibility(View.VISIBLE);
				sendButton.animate().alpha(1f).setDuration(500L).setListener(new AnimatorListenerAdapter() {

					@Override
					public void onAnimationStart(Animator animation) {
						sendButton.setVisibility(View.VISIBLE);
					}
				});
			}
		});
		sendButton.setOnClickListener(v -> {
			channel.getMessages().add(new Message(input.getText().toString(), activity.self, System.currentTimeMillis()));
			mChatAdapter.addMessage(new Message(input.getText().toString(), activity.self, System.currentTimeMillis()));
			input.setText("");
			new Handler().postDelayed(() -> {
				User user = new User(1, "Jasmine", "Don't leaf me alone in quarantine! I'm rooting for everyone", R.mipmap.flower2);
				channel.getMessages().add(new Message("Hello,", user, System.currentTimeMillis()));
				mChatAdapter.addMessage(new Message("Hello,", user, System.currentTimeMillis()));
				new Handler().postDelayed(() -> {
					channel.getMessages().add(new Message("how are you?", user, System.currentTimeMillis()));
					mChatAdapter.addMessage(new Message("how are you?", user, System.currentTimeMillis()));
				}, 2000);
			}, 2700);
		});
		return root;
	}

}