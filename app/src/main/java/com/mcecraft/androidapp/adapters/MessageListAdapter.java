package com.mcecraft.androidapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mcecraft.androidapp.R;
import com.mcecraft.androidapp.Utils.Utils;
import com.mcecraft.androidapp.model.Message;

import java.util.ArrayList;
import java.util.List;

public class MessageListAdapter extends RecyclerView.Adapter {
	private static final int VIEW_TYPE_MESSAGE_SENT = 1;
	private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

	private Context mContext;
	private List<Message> mMessageList = new ArrayList<>();

	public MessageListAdapter(Context context) {
		mContext = context;
	}

	public void addMessage(Message m) {
		mMessageList.add(m);
		notifyDataSetChanged();
	}

	public void addMessages(List <Message> m) {
		mMessageList.addAll(m);
		notifyDataSetChanged();
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view;

		if (viewType == VIEW_TYPE_MESSAGE_SENT) {
			view = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.sender_message, parent, false);
			return new SentMessageHolder(view);
		} else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
			view = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.receiver_message, parent, false);
			return new ReceivedMessageHolder(view);
		}

		return null;
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
		Message message = mMessageList.get(position);

		switch (holder.getItemViewType()) {
			case VIEW_TYPE_MESSAGE_SENT:
				((SentMessageHolder) holder).bind(message);
				break;
			case VIEW_TYPE_MESSAGE_RECEIVED:
				((ReceivedMessageHolder) holder).bind(message);
		}
	}

	@Override
	public int getItemViewType(int position) {
		Message message = mMessageList.get(position);

		//TODO get self
		if (message.getSender().getUserId() == 0) {
			// If the current user is the sender of the message
			return VIEW_TYPE_MESSAGE_SENT;
		} else {
			// If some other user sent the message
			return VIEW_TYPE_MESSAGE_RECEIVED;
		}
	}

	@Override
	public int getItemCount() {
		return mMessageList.size();
	}

	private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
		TextView messageText, timeText, nameText;
		ImageView profileImage;

		ReceivedMessageHolder(View itemView) {
			super(itemView);
			messageText = itemView.findViewById(R.id.text_message_body);
			timeText = itemView.findViewById(R.id.text_message_time);
			nameText = itemView.findViewById(R.id.text_message_name);
			profileImage = itemView.findViewById(R.id.image_message_profile);
		}

		void bind(Message message) {
			messageText.setText(message.getMessage());

			// Format the stored timestamp into a readable String using method.
			timeText.setText(Utils.formatDateTime(message.getCreatedAt()));
			nameText.setText(message.getSender().getNickname());

			// Insert the profile image from the URL into the ImageView.
			profileImage.setImageResource(message.getSender().getProfilePic());
		}
	}

	private class SentMessageHolder extends RecyclerView.ViewHolder {
		TextView messageText, timeText;

		SentMessageHolder(View itemView) {
			super(itemView);
			messageText = itemView.findViewById(R.id.text_message_body);
			timeText = itemView.findViewById(R.id.text_message_time);
		}

		void bind(Message message) {
			messageText.setText(message.getMessage());

			// Format the stored timestamp into a readable String using method.
			timeText.setText(Utils.formatDateTime(message.getCreatedAt()));
		}
	}
}
