package com.mcecraft.androidapp.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.mcecraft.androidapp.MainActivity;
import com.mcecraft.androidapp.R;
import com.mcecraft.androidapp.Utils.Utils;
import com.mcecraft.androidapp.model.Channel;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ChatListAdapter extends RecyclerView.Adapter {

	private Context mContext;
	private List<Channel> mChannelList = new ArrayList<>();

	public ChatListAdapter(Context context) {
		mContext = context;
	}

	public void addChannel(Channel m) {
		mChannelList.add(m);
		notifyDataSetChanged();
	}

	public void addChannels(List<Channel> m) {
		mChannelList.addAll(m);
		notifyDataSetChanged();
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.chat, parent, false);
		return new ChatHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
		Channel channel = mChannelList.get(position);
		((ChatHolder) holder).bind(channel);
	}

	@Override
	public int getItemCount() {
		return mChannelList.size();
	}

	private class ChatHolder extends RecyclerView.ViewHolder {
		TextView chatName, lastMessage, lastMessageSentBy, lastMessageSentAt;
		ImageView chatImage;

		ChatHolder(View itemView) {
			super(itemView);
			chatName = itemView.findViewById(R.id.chatName);
			lastMessage = itemView.findViewById(R.id.message);
			lastMessageSentBy = itemView.findViewById(R.id.name);
			lastMessageSentAt = itemView.findViewById(R.id.date);
			chatImage = itemView.findViewById(R.id.roundedImage);
		}

		void bind(final Channel channel) {
			itemView.setOnClickListener(v -> {
				//ik this is a horrible solution but it works lol
				UUID uuid = UUID.randomUUID();
				((MainActivity)mContext).getTempChannels().put(uuid, channel);
				Bundle bundle = new Bundle();
				bundle.putString("tempChannel", uuid.toString());
				Navigation.findNavController(itemView).navigate(R.id.action_navigation_chats_to_chatFragment, bundle);
			});
			chatName.setText(channel.getName());

			// Format the stored timestamp into a readable String using method.
			if (channel.getMessages().size() != 0) {
				lastMessage.setText(channel.getMessages().get(channel.getMessages().size() - 1).getMessage());
				lastMessageSentBy.setText(channel.getMessages().get(channel.getMessages().size() - 1).getSender().getNickname());
				lastMessageSentAt.setText(Utils.formatDateTime(channel.getMessages().get(channel.getMessages().size() - 1).getCreatedAt()));
			} else {
				lastMessage.setText("");
				lastMessageSentBy.setText("");
				lastMessageSentAt.setText("");
			}
			// Insert the profile image from the URL into the ImageView.
			chatImage.setImageResource(channel.getChatImage());
		}
	}
}
