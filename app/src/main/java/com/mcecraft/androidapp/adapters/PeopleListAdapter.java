package com.mcecraft.androidapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.mcecraft.androidapp.MainActivity;
import com.mcecraft.androidapp.R;
import com.mcecraft.androidapp.model.Channel;
import com.mcecraft.androidapp.model.User;

import java.util.ArrayList;
import java.util.List;

public class PeopleListAdapter extends RecyclerView.Adapter {

	private Context mContext;
	private MainActivity activity = null;
	private List<User> mRecommendedList = new ArrayList<>();

	public PeopleListAdapter(Context context) {
		mContext = context;
	}

	public void addPerson(User m) {
		mRecommendedList.add(m);
		notifyDataSetChanged();
	}

	public void addPeople(List<User> m) {
		mRecommendedList.addAll(m);
		notifyDataSetChanged();
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		activity = (MainActivity) mContext;
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.person, parent, false);
		return new PersonHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
		User channel = mRecommendedList.get(position);
		((PersonHolder) holder).bind(channel);
	}

	@Override
	public int getItemCount() {
		return mRecommendedList.size();
	}

	private class PersonHolder extends RecyclerView.ViewHolder {
		TextView name, description;
		ImageView image;

		PersonHolder(View itemView) {
			super(itemView);
			name = itemView.findViewById(R.id.name);
			description = itemView.findViewById(R.id.userDescription);
			image = itemView.findViewById(R.id.personImage);
		}

		void bind(final User user) {
			name.setText(user.getNickname());
			description.setText(user.getDescription());
			image.setImageResource(user.getProfilePic());
			itemView.setOnClickListener(v -> {
				activity.channels.add(new Channel(user.getProfilePic(), user.getNickname()));
				activity.channelNames.add(user.getNickname());
				Navigation.findNavController(itemView).navigate(R.id.navigation_chats);
			});
		}
	}
}
