package com.mcecraft.androidapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class QuizActivity extends AppCompatActivity {

	String question1 = "What is you favorite subject";
	String[] answers1 = {"Literature", "History", "Math", "Science"};
	String question2 = "How many friends do you have";
	String[] answers2 = {"None, that's why I'm here", "A few close friends", "I have lots of friends.", "Everyone likes me."};

	TextView question;
	Button answer1, answer2, answer3, answer4;

	public void onClick(View v) {
		//Toast.makeText(this, ((Button)v).getLayout().getText(), Toast.LENGTH_LONG).show();
		//TODO
		boolean end = true;
		if (question.getText().equals(question2)) {
			Intent intent = new Intent(this, MainActivity.class);
			intent.putExtra("noQuiz", true);
			startActivity(intent);
		} else {
			question.setText(question2);
			answer1.setText(answers2[0]);
			answer2.setText(answers2[1]);
			answer3.setText(answers2[2]);
			answer4.setText(answers2[3]);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz);
		question = findViewById(R.id.question);
		answer1 = findViewById(R.id.answer1);
		answer2 = findViewById(R.id.answer2);
		answer3 = findViewById(R.id.answer3);
		answer4 = findViewById(R.id.answer4);
		question.setText(question1);
		answer1.setText(answers1[0]);
		answer2.setText(answers1[1]);
		answer3.setText(answers1[2]);
		answer4.setText(answers1[3]);
	}

}