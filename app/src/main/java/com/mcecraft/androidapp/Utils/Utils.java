package com.mcecraft.androidapp.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Utils {

    public static String formatDateTime(long timeInMillis) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        if(dateFormat.format(timeInMillis).equals(dateFormat.format(System.currentTimeMillis()))) {
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("h:mm a", Locale.getDefault());
            return dateFormat2.format(timeInMillis);
        } else {
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMMM dd", Locale.getDefault());
            return dateFormat2.format(timeInMillis);
        }
    }
}
